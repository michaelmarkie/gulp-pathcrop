const 
	through = require('through2'),
    Jimp = require('jimp'),
    Path = require('path'),
	app = require('./package.json'),
	SUPPORTED_EXT = ['png', 'jpg', 'jpeg', 'gif', 'bmp'];


function error (txt, cb) {
	console.error(app.name + ': ' + txt);
	return cb();
}


function autoCrop () {


    // from gulp-rename
    function parsePath(path) {
        var extname = Path.extname(path);
        return {
            dirname: Path.dirname(path),
            basename: Path.basename(path, extname),
            extname: extname
        };
    }

	function bufferContents (file, enc, cb) {

		if (file.isNull())
			return cb();

		if (file.isStream())
			return error('Streaming not supported', cb);


        var 
            rx = /\[([whr0-9,:]+)\]/g,
            parsedPath = parsePath(file.relative),
            match = rx.exec(parsedPath.basename), 
            cleanName = parsedPath.basename.replace(rx, ''),
            path = Path.join(parsedPath.dirname, cleanName + parsedPath.extname),
            height = Jimp.AUTO, 
            width = Jimp.AUTO, 
            ratio = false;

        if( ! match ) {
            // We only want to crop files that request it.
            // Even ones that don't we may as well still pass through the chain.
            this.push(file);
            return cb();
        }

		if ( ! SUPPORTED_EXT.includes(parsedPath.extname.substring(1)) )
            return cb();

        match = match[1].split(',');



        match.forEach( v => {
            switch (v[0]) {
                case 'h':
                    height = parseFloat(v.substring(1));
                    break;
                case 'w':
                    width = parseFloat(v.substring(1));
                    break;
                case 'r':
					if ( ! /^[0-9.]+:[0-9.]+$/g.test(v.substring(1)) )
						return error('Ratio invalid', cb);

                    ratio = v.substring(1).split(':').map(parseFloat);
                    break;
            }
		});

		Jimp.read(file.contents, (err, image) => {

			if ( err ) 
				return error('Error reading ' + file.relative, cb);



			// Cover cant handle auto so we'll set the sizes manually
			if ( width === Jimp.AUTO )
				width = image.bitmap.width;

			if ( height === Jimp.AUTO )
				height = image.bitmap.height;




			// Maintain picture aspect
			if( ! ratio ) {

				// Calculate height based on width
				if ( width != Jimp.AUTO && height === Jimp.AUTO ) 
					height = image.bitmap.height / (image.bitmap.width / width);
				
				// Calculate width based on height
				if ( height != Jimp.AUTO && width === Jimp.AUTO ) 
					width = image.bitmap.width / (image.bitmap.height / height);

			} else {

				height = ( width / ratio[0] ) * ratio[1];
				width = ( height / ratio[1] ) * ratio[0];

			}
			

			image.cover(width, height);


			const format = (( format ) => {
				switch (format) {
					case 'png':
						return Jimp.MIME_PNG;
					case 'gif':
						return Jimp.MIME_GIF;
					case 'bmp':
						return Jimp.MIME_BMP;
					default:
						return Jimp.MIME_JPEG;
				}
			})(parsedPath.extname);

			image.getBuffer(format, (err, buffer) => {

                if (err)
                    return error('Error getting buffer ' + file.relative, cb);

                // Update pipeline
                file.path = Path.join(file.base, path);
                file.contents = buffer;
                
                this.push(file);
                
				cb();

			});
		});
	}

	return through.obj(bufferContents, cb => cb());
};


module.exports = autoCrop;
