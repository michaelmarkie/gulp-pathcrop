# gulp-autocrop

Cloned from my project https://gitlab.com/michaelmarkie/gulp-autocrop/

It will only resize and crop your image any quality adjustments need to be done in another gulp plugin such as imagemin.


### Install
```bash
npm install gulp-pathcrop
```

### Usage
```js
const { src, dest } = require('gulp');
const pathcrop = require('gulp-pathcrop');


function imageTask (cb) {
    src('src/*.png')
        .pipe(pathcrop())
        .pipe(dest('output'))
        .on('finish', cb);
};

exports.default = imageTask;
```

### File Name

myfilename\[w200,h200\].jpg

The above will crop your picture to 200x200

and output:

myfilename.jpg

### Options
If you wish for examples take a look in test/
- w = width 
- h = height
- r = aspect ratio