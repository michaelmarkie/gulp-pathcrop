const { src, dest, series } = require('gulp');
const autocrop = require('./index');


function defaultTask (cb) {

    src('test/*.jpg')
        .pipe(autocrop())
        .pipe(dest('test/output/'))
        .on('finish', cb);
};

exports.default = defaultTask;
